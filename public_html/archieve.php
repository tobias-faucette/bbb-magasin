<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";

$strModuleName = "";


$mode = setMode();

switch (strtoupper($mode)) {

    case "LIST";
        $page = filter_input(INPUT_GET, "page", FILTER_SANITIZE_NUMBER_INT, array("options" => array("default" => 1)));
        require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/header.php";

        /**
         * Pagination
         */

        $limit = 5;
        $start = ($page - 1) * $limit;

        $params = array($start, $limit);
        $sql = "SELECT * FROM news LIMIT ?, ? ";
        $rows = $db->_fetch_array($sql, $params);


        $num_records = $db->_fetch_value("SELECT count(*) from news");
        $num_pages = ceil($num_records / $limit);

        ?>
        <div class="col-sm-8">
            <h1 class="headline"><strong>ARKIVET</strong></h1>

            <div class="row">
                <?php foreach ($rows as $key => $row): ?>
                    <article>
                        <div class="col-sm-12 margin-bot15">
                            <h4><?php echo $row["vcTitle"] ?></h4>
                            <p><i class="fa fa-clock-o" aria-hidden="true"></i>
                                <?php echo date('j F  Y', $row["daCreated"]) . " " . "KL." . date(' G:i', $row["daCreated"]) ?>
                                <i class="fa fa-comments" aria-hidden="true"></i>
                                <i class="fa fa-eye" aria-hidden="true"></i>

                            </p>
                            <p><?php if (strlen($row["txContent"]) > 100) {
                                    $row["txContent"] = substr($row["txContent"], 0, 200) . "...";
                                } else {
                                    $row["txContent"] = $row["txContent"] . "...";
                                }
                                echo $row["txContent"] ?></p>
                            <p><i class="fa fa-tag" aria-hidden="true"></i><?php echo $row["iCatID"] ?></p>
                            <?php echo $arrButtonPanel[] = getButtonLink("", "?mode=details&iNewsID=" . $row["iNewsID"], "Læs Mere", "main-btn"); ?>
                        </div>
                    </article>
                <?php endforeach; ?>

            </div>

            <?php
            $accHtml = "pages";

            for ($i = 1; $i < $num_pages; $i++) {
                $accHtml .= "<a href=\"?page=" . $i . "\">" . $i . "</a>&nbsp;\n";
            }


            echo $accHtml;
            ?>
        </div>

        <?php
        require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/side-bar.php";


        //echo $accHtml;

        require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/footer.php";

        break;

    case
    "DETAILS";
        $iNewsID = filter_input(INPUT_GET, "iNewsID", FILTER_SANITIZE_NUMBER_INT);

        require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/header.php";

        $news = new news();
        $rows = $news->getNews($iNewsID);

        $comment = new comment();
        $row = $comment->newsComment($iNewsID);


        /*
         * News article
         */
        ?>
        <div class="container text-center">
            <div class="row">
                <div class="col-xs-12">
                    <h2> <?php echo $news->vcTitle ?></h2>
                    <hr>
                </div>
                <div class="col-xs-2">
        <span class="fa-stack fa-lg">
        <i class="fa fa-circle fa-stack-2x"></i>
        <a href="javascript:history.go(-1)"> <i class="fa fa-arrow-left fa-stack-1x fa-lg"></i></a>
        </span>
                </div>
                <div class="col-xs-12">
                    <p><?php echo $news->txContent ?></p>
                </div>
                <hr>
            </div>
        </div>


        </section>

        <hr>

        <?php


        if ($row > 0) {

            foreach ($row as $key): ?>
                <div id="display-comments">
                    <div class="col-sm-6 col-sm-offset-3">
                        <div class="panel panel-white post panel-shadow">
                            <div class="post-heading">
                                <div class="pull-left image">
                                    <img src="https://www.placecage.com/c/60/60" class="img-circle avatar"
                                         alt="user profile image">
                                </div>
                                <div class="pull-left meta">
                                    <div class="title h5">
                                        <a href="#"><strong><?php echo $key["vcName"] ?></strong></a>
                                        commented
                                    </div>
                                    <h6 class="text-muted time"><?php echo date2local($key["daCreated"]) ?></h6>
                                </div>
                            </div>
                            <div class="post-description">
                                <h4><strong><?php echo $key["vcTitle"] ?></strong></h4>
                                <p><?php echo $key["txContent"] ?></p>
                                <div class="stats">
                                    <a href="#" class="btn btn-default stat-item">
                                        <i class="fa fa-thumbs-up icon"></i>2
                                    </a>
                                    <a href="#" class="btn btn-default stat-item">
                                        <i class="fa fa-thumbs-down icon"></i>12
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach;
        } ?>

        <div class=" col-sm-12 margin-bot15" class="comment">
            <form method="POST" id="comment_form">
                <hr>
                <input type="hidden" id="iNewsID" name="iNewsID" value="<?php echo $iNewsID ?>">
                <div class="col-sm-12">
                    <label class="">Emne<input type="text" id="vcTitle" class="form-control" required
                                               name="vcTitle"></label>
                </div>
                <div class="col-sm-12">
                    <label class="">Forfatter<input type="text" id="vcName" class="form-control" required
                                                    name="vcName"></label>
                </div>
                <div class="col-sm-12">
                    <label class="">Besked<textarea id="txContent" name="txContent"
                                                    class="form-control" required></textarea></label>
                </div>
                <div class="col-sm-12">
                    <button class="sign-up-btn" id="submit_comment" type="submit">Publicer</button>

                </div>
            </form>
        </div>

        <?php

        require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/footer.php";
        break;
}

