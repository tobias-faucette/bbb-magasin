<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";
$mode = setMode();


switch (strtoupper($mode)) {

    case "LIST";
        require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/header.php";

        $news = new news();
        $rows = $news->getAllNews(6);



        $comment = new comment();
        $iNewsID = $news->iNewsID;



        ?>
        <div class="col-sm-8">
            <h1 class="headline">SENESTE ARTIKLER</h1>

            <div class="row">
                <?php foreach ($rows as $key => $row): ?>
                    <article>
                        <div class="col-sm-6 margin-bot15">
                            <h4><?php echo $row["vcTitle"] ?></h4>
                            <p><i class="fa fa-clock-o" aria-hidden="true"></i>
                                <?php echo date('j F  Y', $row["daCreated"]) . " " . "KL." . date(' G:i', $row["daCreated"]) ?>
                                <i class="fa fa-comments"
                                   aria-hidden="true">&nbsp;<?php echo $comment->commentCount($row["iNewsID"]) ?>
                                    KOMMENTARER</i>
                                <i class="fa fa-eye" aria-hidden="true">&nbsp;<?php echo $row["iViews"] ?>&nbsp;Visninger</i>

                            </p>
                            <p><?php if (strlen($row["txContent"]) > 100) {
                                    $row["txContent"] = substr($row["txContent"], 0, 200) . "...";
                                } else {
                                    $row["txContent"] = $row["txContent"] . "...";
                                }
                                echo $row["txContent"] ?></p>
                            <p><i class="fa fa-tag" aria-hidden="true"></i><?php "" ?></p>

                            <?php echo $arrButtonPanel[] = getButtonLink("", "?mode=details&iNewsID=" . $row["iNewsID"], "Læs Mere", "main-btn"); ?>
                        </div>
                    </article>
                <?php endforeach; ?>

            </div>
        </div>

        <?php
        require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/side-bar.php";


        require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/footer.php";

        break;

    case "DETAILS";
        $iNewsID = filter_input(INPUT_GET, "iNewsID", FILTER_SANITIZE_NUMBER_INT);
        require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/header.php";

        $news = new news();
        $rows = $news->getNews($iNewsID);


        /*
         * new instance of comment class
         * call news comment function
         */
        $comment = new comment();
        $row = $comment->newsComment($iNewsID);

        $num_comments = $db->_fetch_value("SELECT count(*) FROM comment WHERE iNewsID = $iNewsID");


        /**
         * Article view count
         *
         */
        $sql = "UPDATE news set iViews=iViews+1 where iNewsID = $iNewsID";
        $db->_query($sql);

        $result = $db->_fetch_value("SELECT iViews from news where iNewsID = $iNewsID");


        ?>
        <div class="col-sm-8">
            <h1 class="headline"><?php echo $news->vcTitle ?></h1>
            <p><i class="fa fa-clock-o" aria-hidden="true"></i>
                <?php echo date('j F  Y', $news->daCreated) . " " . "KL." . date(' G:i', $news->daCreated) ?>
                <i class="fa fa-comments"
                   aria-hidden="true">&nbsp;<?php echo $num_comments ?></i>
                <i class="fa fa-eye" aria-hidden="true">&nbsp;<?php echo $result ?></i>

            </p>
            <p><?php echo $news->txContent ?></p>
            <h1 class="headline">KOMMENTARER</h1>
            <?php
            /**
             * comment section
             */

            if ($row > 0) {
                foreach ($row as $key): ?>
                    <div class="col-sm-1">
                        <i class="fa fa-comment fa-2x" aria-hidden="true"></i>
                    </div>
                    <div class="col-sm-11">
                        <h4><?php echo $key["vcTitle"] ?></h4>
                        <p><i class="fa fa-clock-o" aria-hidden="true"></i>
                            <?php echo date('j F  Y', $key["daCreated"]) . " " . "KL." . date(' G:i', $key["daCreated"]) ?>
                        <p><?php echo $key["txContent"] ?></p>
                        <hr>
                    </div>
                <?php endforeach;
            } ?>
            <div class="col-sm-8 comment">
                <form method="POST" id="comment_form">
                    <input type="hidden" id="iNewsID" name="iNewsID" value="<?php echo $iNewsID ?>">
                    <div class="col-sm-6">
                        <label class="">Dit Navn<input type="text" id="vcTitle" class="form-control" required
                                                       name="vcTitle"></label>
                    </div>
                    <div class="col-sm-6">
                        <label class="">Din E-mailadresse<input type="text" id="vcName" class="form-control" required
                                                                name="vcName">
                        </label>
                    </div>
                    <div class="col-sm-12">
                        <label class="">Kommentar<textarea id="txContent" name="txContent"
                                                           class="form-control" required></textarea></label>
                    </div>
                    <div class="col-sm-4 margin-bot15">
                        <button class="sign-up-btn main-btn form-control" id="submit_comment" type="submit">Udfør
                        </button>

                    </div>
                </form>
            </div>
        </div>


        <?php
        require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/side-bar.php";

        require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/footer.php";

        break;

}


