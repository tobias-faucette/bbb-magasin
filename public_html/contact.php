<?php
/**
 * Created City PhpStorm.
 * User: tn116
 * Date: 24-08-2017
 * Time: 22:01
 */

require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";

require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/header.php";
$iOrgID = filter_input(INPUT_GET, "iOrgID", FILTER_SANITIZE_NUMBER_INT);

$org = new org();
$org->getOrg(7);
?>
    <div class="col-sm-8">
        <h1 class="headline">KONTAKT MAGASINET</h1>
        <div class="contact">
            <div class="col-sm-6 padding-zero">
                <ul class="padding-zero">
                    <h3><i class="fa fa-map-marker" aria-hidden="true"></i>
                        Adresse</h3>
                    <li><?php echo $org->vcAddress ?></li>
                    <li><?php echo $org->iZip . " " . $org->vcCity ?></li>
                    <li><?php echo $org->vcCountry ?></li>
                </ul>
            </div>
            <div class="col-sm-6">
                <ul>
                    <h3>Kontaktoplysninger</h3>
                    <li><i class="fa fa-phone" aria-hidden="true"></i>
                        Telefon: <?php echo $org->vcPhone ?>
                    </li>
                    <li><i class="fa fa-fax" aria-hidden="true"></i>
                        Fax: <?php echo $org->vcFax ?>
                    </li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i>
                        E-mail: <?php echo $org->vcEmail ?>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-sm-12 padding-zero">
            <form method="POST" id="contact-form" action="assets/scripts/contact-message.php">
                <fieldset><h2>Kontakt Formular</h2></fieldset>
                <div class="col-sm-6">
                    <label class="">Dit Navn<input type="text" id="vcName" class="form-control" required
                                                   name="vcName"></label>
                </div>
                <div class="col-sm-6">
                    <label class="">Din E-mailadresse<input type="text" id="vcEmail" class="form-control" required
                                                            name="vcEmail">
                    </label>
                </div>
                <div class="col-sm-6">
                    <label class="">Emne<input type="text" id="vcSubject" class="form-control" required
                                               name="vcSubject">
                    </label>
                </div>
                <div class="col-sm-12">
                    <label>Din Besked<textarea id="txContent" name="txContent"
                                               class="form-control" required></textarea></label>
                </div>
                <div class="col-sm-4 margin-bot15">
                    <button class="sign-up-btn main-btn form-control" id="submit_comment" type="submit">
                        <?php echo strtoupper("Send Besked") ?>
                    </button>

                </div>
            </form>
        </div>
    </div>

<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/side-bar.php";

require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/footer.php";
