<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/header.php";


$server = "localhost";
$username = "root";
$password = "";
$dbname = "bbb-magasin";

$conn = mysqli_connect($server, $username, $password, $dbname);

$product = new shopproduct();
?>


    <div class="container">
        <h1>Her er de resultater der matchede din søgning</h1>
        <?php
        if (isset($_GET['search'])) {
            $search = mysqli_real_escape_string($conn, $_GET['search']);
            $sql = "SELECT * FROM shopproduct WHERE vcTitle LIKE '%$search%' OR txShortDesc LIKE '%$search%' OR txLongDesc LIKE '%$search%' OR iPrice LIKE '%$search%'";
            $result = mysqli_query($conn, $sql);
            $queryResult = mysqli_num_rows($result);


            if ($queryResult > 0) {
                echo "We found " . $queryResult . " Match";

                while ($row = mysqli_fetch_assoc($result)) { ?>
                    <div class="row spacing">
                        <div class="col-md-10">
                            <section class="col-xs-12 col-sm-6 col-md-12">
                                <article class="search-result row">
                                    <div class="col-xs-12 col-sm-12 col-md-3">
                                        <a href="<?php echo "index.php?mode=details&iProductID=" . $row["iProductID"] ?>"
                                           title="Lorem ipsum" class="thumbnail"><img
                                                    src="images/<?php echo $row['vcImage1'] ?>" alt="Lorem ipsum"/></a>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-5 excerpet">
                                        <h4><strong><?php echo $row['vcTitle'] ?></strong></h4>
                                        <p><?php echo $row['txShortDesc'] ?></p>
                                        <p><?php echo $row['iStock'] ?> stk på lager</p>
                                        <p>Varenummer <?php echo $row['vcProductNumber'] ?></p>

                                    </div>
                                    <span class="clearfix borda"></span>
                                </article>
                            </section>

                        </div>
                    </div>
                    <?php
                }
            } else {
                echo "No match found.";
            }
        }

        ?>
    </div>

    <!--  echo "<div class='row'>
        <h3>" . $row['vcTitle'] . "</h3>
        <h3>" . $row['txShortDescription'] . "</h3>
    </div>";-->
<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/footer.php";
