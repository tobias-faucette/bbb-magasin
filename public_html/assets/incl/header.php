<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";

?>

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php projectName() ?></title>
    <link rel="icon" type="image/png" href="images/favicon-32x32.png">
    <meta name="viewport" content="width=device-width" initial-scale="1"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/css/main.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/css/shop.css" rel="stylesheet" type="text/css"/>


</head>
<body>

<div class="container content-bg">
    <div class="row">
        <div class="col-sm-12 padding-zero logo">
            <img src="images/bbb-logo.png" class="img-responsive">

        </div>
    </div>
    <div class="row margin-bot15 ">
        <div class="col-sm-12 padd nav-col">
            <div class="col-sm-12 padding-zero">
                <nav class="navbar-gray padd ">
                    <ul class="nav navbar-nav">
                        <li><a href="index.php"><i class="fa fa-home" aria-hidden="true"></i>
                                Home</a></li>
                        <li><a href="car.php">BILER</a></li>
                        <li><a href="boat.php">BÅDE</a></li>
                        <li><a href="bike.php">BIKE'S</a></li>
                        <li><a href="archieve.php">ARKIVET</a></li>
                        <li><a href="contact.php">KONTAKT</a></li>
                        <li><a href="editorial.php">REDAKTIONEN</a></li>
                        <li><a href="shop.php">shop</a></li>
                        <li class="">
                            <a href="cart.php"><i
                                        class="fa fa-fw fa-shopping-cart"></i>Kurv<span
                                        id="productsInCart"> <?php echo $cart->getCartQuantity(); ?></span></a>
                        </li>
                    </ul>
            </div>
            </nav>
        </div>
    </div>
