<?php
/**
 * Created by PhpStorm.
 * User: tn116
 * Date: 04-09-2017
 * Time: 10:33
 * Comment PHP script
 * Author Tobias N.
 *
 */
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";


$iNewsID = filter_input(INPUT_POST, "iNewsID", FILTER_SANITIZE_NUMBER_INT);
$vcTitle = filter_input(INPUT_POST, "vcTitle", FILTER_SANITIZE_STRING);
$vcName = filter_input(INPUT_POST, "vcName", FILTER_SANITIZE_STRING);
$txContent = filter_input(INPUT_POST, "txContent", FILTER_SANITIZE_STRING);

if (isset($_POST['done'])) {

    //Saves data to database


    $params = array(
        $iNewsID,
        $vcTitle,
        $vcName,
        $txContent,
        time()
    );


    $sql = "INSERT into comment (" .
        "iNewsID, " .
        "vcTitle, " .
        "vcName, " .
        "txContent, " .
        "daCreated) " .
        "VALUES(?,?,?,?,?)";


    $db->_query($sql, $params);


}


//Display comments from database

if (isset($_POST['display'])) {

    $comment = new comment();
    $row = $comment->newsComment($iNewsID);


    if ($row > 0) {

        foreach ($row as $key): ?>
            <div id="display_comments">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="panel panel-white post panel-shadow">
                        <div class="post-heading">
                            <div class="pull-left image">
                                <img src="https://www.placecage.com/c/60/60" class="img-circle avatar"
                                     alt="user profile image">
                            </div>
                            <div class="pull-left meta">
                                <div class="title h5">
                                    <a href="#"><strong><?php echo $key["vcName"] ?></strong></a>
                                    commented
                                </div>
                                <h6 class="text-muted time"><?php echo date2local($key["daCreated"]) ?></h6>
                            </div>
                        </div>
                        <div class="post-description">
                            <h4><strong><?php echo $key["vcTitle"] ?></strong></h4>
                            <p><?php echo $key["txContent"] ?></p>
                            <div class="stats">
                                <a href="#" class="btn btn-default stat-item">
                                    <i class="fa fa-thumbs-up icon"></i>2
                                </a>
                                <a href="#" class="btn btn-default stat-item">
                                    <i class="fa fa-thumbs-down icon"></i>12
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach;
    }


}