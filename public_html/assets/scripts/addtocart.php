<?php require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/webshop/public_html/assets/incl/init.php";
$iProductID = (int)filter_input(INPUT_POST, "iProductID", FILTER_SANITIZE_NUMBER_INT, array("options" => array("min_range" => 1)));

$iQuantity = (int)filter_input(INPUT_POST, "quantity", FILTER_SANITIZE_NUMBER_INT, array("options" => array("min_range" => 1)));

if (!$iProductID || !$iQuantity) {
    exit();
}

if (!isset($cart->iCartID)) {
    $cart->iCartID = $cart->create($auth->iUserID);
}

$cart->removeProduct($iProductID);
$cart->addProduct($iProductID, $iQuantity);

$arrJson = array(
    "iProductID" => $iProductID,
    "productsInCart" => $cart->getCartQuantity(),

);

echo json_encode($arrJson);

