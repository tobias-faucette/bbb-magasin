<?php require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/webshop/public_html/assets/incl/init.php";
$iCartLineID = (int)filter_input(INPUT_POST, "iCartLineID", FILTER_SANITIZE_NUMBER_INT);

if (!isset($cart->iCartID)) {
    $cart->iCartID = $cart->removeProduct($auth->iUserID);
}


$sql = "DELETE FROM shopcartline WHERE iCartLineID = ? AND iCartID = ?";
$db->_query($sql, array($iCartLineID, $cart->iCartID));


$arrJson = array(
    "iCartLineID" => $iCartLineID,
    "iCartTotal" => $cart->getCartTotal(),
    "productsInCart" => $cart->getCartQuantity()

);

echo json_encode($arrJson);
