/**
 * post & display comment
 */

$(document).ready(function () {
    //alert("shit works");

    $("#submit_comment").click(function () {
        var news = $("#iNewsID").val();
        var title = $("#vcTitle").val();
        var name = $("#vcName").val();
        var comment = $("#txContent").val();


        // Callback
        $.ajax({
            url: "assets/scripts/comment.php",
            type: "POST",
            data: {

                done: 1,
                "iNewsID": news,
                "vcTitle": title,
                "vcName": name,
                "txContent": comment,
            },
            success: function (data) {
                //console.log("worked");
                displayComment();
                $("#iNewsID").val();
                $("#vcTitle").val();
                $("#vcName").val();
                $("#txContent").val();


            }
        })
    });


});

function displayComment() {
    $.ajax({

        url: "assets/scripts/comment.php",
        type: "POST",
        data: {
            "display": 1
        },

        success: function (d) {
            $("#display_comments").html(d);
            //console.log("displayed comment worked");
        }

    });
}


/**
 * function to check username exists
 */

$(function () {
    $("[data-check-username]").bind("blur", function () {
        $("#loader").show();
        $.ajax({
            url: "assets/scripts/checkuser.php",
            data: 'vcUserName=' + $("#vcUserName").val(),
            type: "POST",
            success: function (data) {
                if (data == "1") {
                    $("#user-availability-status")
                        .html("Username Not Available.")
                        .removeClass('available')
                        .addClass('not-available');

                } else {
                    $("#user-availability-status")
                        .html("Username Available.")
                        .removeClass('not-available')
                        .addClass('available');
                }
                $("#loader").hide();
            },
            error: function () {
            }
        });
    });
});

/**
 * add to cart function
 */

$(".addtocart").click(function () {
    $.ajax({
        type: "POST",
        url: "assets/scripts/addtocart.php",
        data: "iProductID=" + $(this).data("product") + "&quantity=" + $(this).next().val(),
        success: function (result) {
            var obj = $.parseJSON(result);
            $("#product" + obj.iProductID + " .isincart").text("Dette produkt ligger i kurven");
            if ($("#productsInCart").val() === 0) {
                $("#productsInCart").text("");
            } else {
                $("#productsInCart").text(obj.productsInCart);

            }
            console.log(obj);

        }
    });
});


/**
 * Remove cart product function
 */

$(".remove").click(function () {
    $.ajax({
        type: "POST",
        url: "assets/scripts/removeproduct.php",
        data: "iCartLineID=" + $(this).data("cartlineid"),
        success: function (result) {
            var obj = $.parseJSON(result);
            $("#cartline" + obj.iCartLineID).remove();
            $('#cartline' + obj.iCartLineID + " .total").text(obj.iCartTotal);
            $("#productsInCart").text(obj.productsInCart); //updates number of products in cart, in the menu
            $(".total").text(obj.iCartTotal); //updates total sum of products in cart


        }
    })
})


/**
 * update function for cart
 * number of product, price, total.
 * gets html element City class
 */

$(".update").click(function () {
    $.ajax({
        type: "POST",
        url: "assets/scripts/updateproduct.php",
        data: "iCartLineID=" + $(this).data('cartlineid') + "&iQuantity=" + $('#quantity' + $(this).data('cartlineid')).val(),
        success: function (result) {
            //console.log(result);
            var obj = $.parseJSON(result);
            $("#product" + obj.iQuantity).update;
            price = $('#cartline' + obj.iCartLineID + " .price").text(); // price per prodcuct
            price = parseFloat(price);
            priceSum = price * $('#quantity' + obj.iCartLineID).val(); // price * quantity
            priceSum = priceSum.toLocaleString("da", {minimumFractionDigits: 2});
            $('#cartline' + obj.iCartLineID + " .itemtotal").text(priceSum);  //total of prodcuts in cart
            $(".total").text(obj.iCartTotal);
            $("#productsInCart").text(obj.productsInCart); //updates number of products in cart, in the menu

            ;

        }
    })

})
