/**
 * Created City tn116 on 19-04-2017.
 */

function validate(formObj) {
    var result = 0;

    $.each($(formObj).find(":enabled"), function (index, value) {

        if ($(this).data("required")) {
            switch (this.type) {
                case "text":
                case "password":
                case "textarea":
                case "email":
                    if (!$(this).val()) {
                        result = 0;
                        showError($(this), "Feltet må ikke være tomt");
                        $(this).bind("keydown", function () {
                            removeError($(this));
                            result = 1;
                        })
                        return false;
                    }
                    break;

                case "checkbox":
                    if (!$(this).is(":checked")) {
                        showError($(this), "Du skal Acceptere vores betingelser");
                        result = 0;
                        return false;
                    } else {
                        removeError($(this));
                        result = 1;
                    }
                    break;
            }


        }

        // Validation switch

        switch ($(this).data("validate")) {

            case "validText":
                if (!isValidAlpha($(this).val(), 2, 20)) {
                    showError($(this), "Feltet må kun indeholde bogstaver!");
                    result = 0;
                    return false;
                } else {
                    removeError($(this));
                    result = 1;
                }
                break;
            case "password":
                if (!isValidLength($(this).val(), 8, 20)) {
                    showError($(this), "Adgangskode skal være mellem 8 og 20");
                    result = 0;
                    return false;
                } else {
                    removeError($(this));
                    result = 1;
                }
                break;

            case "passwordMatch":
                /*
                 var match = $(this).data("match")
                 if($(this).val() != $("#"+match).val()) {
                 }
                 */
                if ($("#password").val() != ($("#password_Match")).val()) {
                    showError($(this), "Password er ikke ens");
                    result = 0;
                    return false;
                }
                else {
                    removeError($(this));
                    result = 1;
                }
                break;
            case "validemail":
                if (!isValidEmail($(this).val(), 8, 20)) {
                    showError($(this), "Dette er ikke en gyldig email adresse");
                    result = 0;
                    return false;
                } else {
                    removeError($(this));
                    result = 1;
                }
                break;
            case "number":
                if (!isValidNumber($(this).val(), 8, 20)) {
                    showError($(this), "Indtast et gyldigt telefon nummer");
                    result = 0;
                    return false;
                } else {
                    removeError($(this));
                    result = 1;
                }
                break;


        }

    });

    if (result) {
        formObj.submit();
    }
}


function showError(elm, msg) {
    if (!elm.next().hasClass("text-danger")) {
        $(elm).after("<span class=\"small text-danger\">" + msg + "</span>");
        $(elm).parent().addClass("has-error");
    }
}

function removeError(elm) {
    if ($(elm).next().hasClass("text-danger")) {
        $(elm).next().remove();
        $(elm).parent().removeClass("has-error");
    }
}

/* RegEx & Matching functions  */

//Tjekker om værdi er et nummer
function isValidNumber(value) {
    var pattern = /^[0-9]+$/;
    return pattern.test(value);
}

//Tjekker om værdi er alfabet
function isValidAlpha(value) {
    var pattern = /^[A-ZÆØÅa-zæøå ]+$/;
    return pattern.test(value);
}

//Tjekker om værdi har en gyldig email syntaks
function isValidEmail(value) {
    var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return pattern.test(value);
}

//Tjekker at værdi har en gyldig lændge
function isValidLength(value, min, max) {
    var pattern = RegExp('^[0-9A-Za-z@#$%]{' + min + ',' + max + '}$');
    return pattern.test(value);
}