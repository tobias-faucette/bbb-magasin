<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/header.php";
?>


    <div class="col-lg-10">
        <section id="profile-front">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-11 text-center">
                        <h1 class="animate-reveal animate-first">Velkommen til din profil</h1>
                        <hr class="style1 animate-reveal animate-second">
                        <h3 class="animate-reveal animate-third">Herunder kan du se din besparelse</h3>
                    </div>
                </div>
            </div>
        </section>

        <section id="profile-text" class="whitespace">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-11">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h2 class="panel-title">
                                    <strong><?php echo $auth->user->vcFirstName . " " . $auth->user->vcLastName ?></strong>
                                </h2>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" align="center"><img alt="User Pic"
                                                                                       src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
                                                                                       class="img-circle img-responsive">
                                    </div>
                                    <div class="col-md-9 col-lg-9">
                                        <table class="table table-user-information">
                                            <tbody>
                                            <tr>
                                                <td>Brugernavn:</td>
                                                <td><?php echo $auth->user->vcUserName ?></td>
                                            </tr>
                                            <tr>
                                                <td>Bruger siden:</td>
                                                <td><?php echo date("d. M Y H:i:s", $auth->user->daCreated) ?></td>
                                            </tr>
                                            <tr>
                                                <td>Fornavn:</td>
                                                <td><?php echo $auth->user->vcFirstName ?></td>
                                            </tr>
                                            <tr>
                                                <td>Efternavn:</td>
                                                <td><?php echo $auth->user->vcLastName ?></td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td><a href="#"><?php echo $auth->user->vcEmail ?></a></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <a href="register.php?mode=register&iUserID=<?php echo $auth->iUserID ?>"
                                   data-original-title="Edit this user" data-toggle="tooltip" type="button"
                                   class="btn                                   btn-sm btn-success"><i
                                            class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
    </div>
    </div>

    </section>
    </div>
    </div>

<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/footer.php";
