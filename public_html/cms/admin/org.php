<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";

$mode = setMode();

$strModuleName = "Organization";

switch (strtoupper($mode)) {

    case "LIST";
        $strModuleMode = "Overview";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButton("button", "New", "getUrl('?mode=edit&iOrgID=-1')", "btn-success");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        $org = new org();

        /* Array with fields and friendly names for list purposes*/
        $arrColumns = array(
            "opts" => "Options",
            "vcOrgName" => "Firm Name",
            "vcCity" => "City",
        );

        /* Array for all event rows */
        $arrOgs = array();

        /* List orgs and set editing options */
        foreach ($org->getlist() as $key => $arrValues) {
            $arrValues["opts"] = getIcon("?mode=details&iOrgID=" . $arrValues["iOrgID"], "eye") .
                getIcon("?mode=edit&iOrgID=" . $arrValues["iOrgID"], "pencil") .
                getIcon("", "trash", "Slet Organisation", "remove(" . $arrValues["iOrgID"] . ")");

            /* Add value row to arrUsers */
            $arrOgs[] = $arrValues;
        }

        /* Call list presenter object with columns (arrColumns) and rows (arrOgs) */
        $p = new listPresenter($arrColumns, $arrOgs);
        echo $p->presentlist();

        sysFooter();
        break;

    case "DETAILS";
        $iOrgID = filter_input(INPUT_GET, "iOrgID", FILTER_SANITIZE_NUMBER_INT);

        $strModuleMode = "Details";

        sysHeader();

        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Overview", "btn-primary");
        $arrButtonPanel[] = getButtonLink("pencil", "?mode=edit&iOrgID=" . $iOrgID, "Edit Event", "btn-success");

        echo textpresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);


        $org = new org();
        $org->getOrg($iOrgID);


        $arrValues = get_object_vars($org);

        /*Converts date/stamp to readable date */
        $arrValues["daCreated"] = date2local($arrValues["daCreated"]);


        $presenter = new listPresenter($org->arrLabels, $arrValues);
        echo $presenter->presentdetails();


        sysFooter();
        break;

    case "EDIT";
        $iOrgID = (int)filter_input(INPUT_GET, "iOrgID", FILTER_SANITIZE_NUMBER_INT);
        $strModuleMode = "Details";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButton("button", "Details", "getUrl('?mode=details&iOrgID=" . $iOrgID . "'),");
        $arrButtonPanel[] = getButton("button", "New", "getUrl('?mode=edit&iOrgID=-1')");
        $arrButtonPanel[] = getButton("button", "Overview", "getUrl('?mode=list')");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        /* Create class instance and set current org */
        $org = new org();

        /* Get org if state = update */
        if ($iOrgID > 0) {
            $org->getOrg($iOrgID);
        }

        /* Get property values */
        $arrValues = get_object_vars($org);


        /* Create presenter instance and set form */
        $form = new formpresenter($org->arrLabels, $org->arrFormElms, $arrValues);
        echo $form->presentForm();

        sysFooter();
        break;

    case "SAVE":
        $org = new org();

        /*
         * Loop form elements from org class & set org property if exists
         * Otherwise set default value from form elements
         * (Defined in org class)
         */
        foreach ($org->arrFormElms as $field => $arrTypes) {
            $org->$field = filter_input(INPUT_POST, $field, $arrTypes[1], getDefaultValue($arrTypes[3]));
        }

        $org->daStart = makeStamp("daStart");
        $org->daStop = makeStamp("daStop");

        /* Save method*/
        $iOrgID = $org->save();
        header("Location: ?mode=details&iOrgID=" . $iOrgID);
        break;

    case "DELETE":
        $org = new org();
        $org->iOrgID = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);
        $org->delete($iOrgID);
        header("Location: ?mode=list");
        break;


}

require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/footer.php"; ?>
<script src="/assets/js/ajaxFunctions.js"></script>
