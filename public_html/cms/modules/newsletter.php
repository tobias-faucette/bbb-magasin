<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";

$mode = setMode();

$strModuleName = "newsletter";

switch (strtoupper($mode)) {

    case "LIST";
        $strModuleMode = "Overview";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButton("button", "New", "getUrl('?mode=edit&iLetterID=-1')", "btn-success");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        $newsletter = new newsletter();

        /* Array with fields and friendly names for list purposes*/
        $arrColumns = array(
            "opts" => "Options",
            "vcEmail" => "Email",
            "vcName" => "Name",
        );

        /* Array for all newsletter rows */
        $arrnewsletters = array();

        /* List orgs and set editing options */
        foreach ($newsletter->getlist() as $key => $arrValues) {
            $arrValues["opts"] = getIcon("?mode=details&iLetterID=" . $arrValues["iLetterID"], "eye") .
                getIcon("?mode=edit&iLetterID=" . $arrValues["iLetterID"], "pencil") .
                getIcon("", "trash", "Slet newsletter", "remove(" . $arrValues["iLetterID"] . ")");

            /* Add value row to arrUsers */
            $arrnewsletters[] = $arrValues;
        }

        /* Call list presenter object with columns (arrColumns) and rows (arrnewsletters) */
        $p = new listPresenter($arrColumns, $arrnewsletters);
        echo $p->presentlist();

        sysFooter();
        break;

    case "DETAILS";
        $iLetterID = filter_input(INPUT_GET, "iLetterID", FILTER_SANITIZE_NUMBER_INT);

        $strModuleMode = "Details";

        sysHeader();

        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Overview", "btn-primary");
        $arrButtonPanel[] = getButtonLink("pencil", "?mode=edit&iLetterID=" . $iLetterID, "Edit newsletter", "btn-success");

        echo textpresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);


        $newsletter = new newsletter();
        $newsletter->getNews($iLetterID);


        $arrValues = get_object_vars($newsletter);


        $presenter = new listPresenter($newsletter->arrLabels, $arrValues);
        echo $presenter->presentdetails();


        sysFooter();
        break;

    case "EDIT";
        $iLetterID = (int)filter_input(INPUT_GET, "iLetterID", FILTER_SANITIZE_NUMBER_INT);
        $strModuleMode = "Details";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButton("button", "Details", "getUrl('?mode=details&iLetterID=" . $iLetterID . "'),");
        $arrButtonPanel[] = getButton("button", "New", "getUrl('?mode=edit&iLetterID=-1')");
        $arrButtonPanel[] = getButton("button", "Overview", "getUrl('?mode=list')");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        /* Create class instance and set current org */
        $newsletter = new newsletter();

        /* Get org if state = update */
        if ($iLetterID > 0) {
            $newsletter->getNews($iLetterID);
        }

        /* Get property values */
        $arrValues = get_object_vars($newsletter);


        /* Create presenter instance and set form */
        $form = new formpresenter($newsletter->arrLabels, $newsletter->arrFormElms, $arrValues);
        echo $form->presentForm();

        sysFooter();
        break;

    case "SAVE":
        $newsletter = new newsletter();



        foreach ($newsletter->arrFormElms as $field => $arrTypes) {
            $newsletter->$field = filter_input(INPUT_POST, $field, $arrTypes[1], getDefaultValue($arrTypes[3]));
        }

        /* Save method*/
        $iLetterID = $newsletter->save();
        header("Location: ?mode=details&iLetterID=" . $iLetterID);
        break;

    case "DELETE":
        $newsletter = new newsletter();
        $newsletter->iLetterID = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);
        $newsletter->delete($iLetterID);
        header("Location: ?mode=list");
        break;


}

require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/footer.php"; ?>
<script src="/assets/js/ajaxFunctions.js"></script>
