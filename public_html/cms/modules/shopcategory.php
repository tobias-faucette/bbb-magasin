<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";

$mode = setMode();

$strModuleName = "Catagory";

switch (strtoupper($mode)) {

    case "LIST";
        $strModuleMode = "Overview";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButton("button", "New", "getUrl('?mode=edit&iCategoryID=-1')", "btn-success");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        $category = new shopcategory();

        /* Array with fields and friendly names for list purposes*/
        $arrColumns = array(
            "opts" => "Options",
            "vcTitle" => "Shop Category",
            "iIsActive" => "Active",
        );

        /* Array for all product rows */
        $arrCategories = array();

        /* List orgs and set editing options */
        foreach ($category->getlist() as $key => $arrValues) {
            $arrValues["iIsActive"] = boolToIcon($arrValues["iIsActive"]);
            $arrValues["opts"] = getIcon("?mode=details&iCategoryID=" . $arrValues["iCategoryID"], "eye") .
                getIcon("?mode=edit&iCategoryID=" . $arrValues["iCategoryID"], "pencil") .
                getIcon("", "trash", "Slet product", "remove(" . $arrValues["iCategoryID"] . ")");

            /* Add value row to arrUsers */
            $arrCategories[] = $arrValues;
        }

        /* Call list presenter object with columns (arrColumns) and rows (arrCategories) */
        $p = new listPresenter($arrColumns, $arrCategories);
        echo $p->presentlist();

        sysFooter();
        break;

    case "DETAILS";
        $iCategoryID = filter_input(INPUT_GET, "iCategoryID", FILTER_SANITIZE_NUMBER_INT);

        $strModuleMode = "Details";

        sysHeader();


        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Overview", "btn-primary");
        $arrButtonPanel[] = getButtonLink("pencil", "?mode=edit&iCategoryID=" . $iCategoryID, "Edit Produkt", "btn-success");

        echo textpresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);


        $category = new shopcategory();
        $category->getCategory($iCategoryID);


        $arrValues = get_object_vars($category);

        /*Converts date/stamp to readable date */
        $arrValues["iIsActive"] = boolToIcon($arrValues["iIsActive"]);
        $arrValues["daCreated"] = date2local($arrValues["daCreated"]);


        $presenter = new listPresenter($category->arrLabels, $arrValues);
        echo $presenter->presentdetails();


        sysFooter();
        break;

    case "EDIT";
        $iCategoryID = (int)filter_input(INPUT_GET, "iCategoryID", FILTER_SANITIZE_NUMBER_INT);
        $strModuleMode = "Details";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButton("button", "Details", "getUrl('?mode=details&iCategoryID=" . $iCategoryID . "'), ", "btn-info");
        $arrButtonPanel[] = getButton("button", "New", "getUrl('?mode=edit&iCategoryID=-1')", "btn-success");
        $arrButtonPanel[] = getButton("button", "Overview", "getUrl('?mode=list')", "btn-primary");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        /* Create class instance and set current org */
        $category = new shopcategory();

        /* Get org if state = update */
        if ($iCategoryID > 0) {
            $category->getCategory($iCategoryID);
        }

        /* Get property values */
        $arrValues = get_object_vars($category);

        /* Get orgs as venues */
        $strSelect = "SELECT iParentID, vcTitle FROM shopcategory WHERE iDeleted = 0 ORDER  BY vcTitle";
        $arrCategory = $db->_fetch_array($strSelect);
        /* Add a default value to the selectbox */
        array_unshift($arrCategory, array("iParentID" => 0, "vcTitle" => "Vælg Katogori"));

        $arrValues["iParentID"] = formpresenter::inputSelect("iParentID", $arrCategory, $category->iParentID);


        /* Create presenter instance and set form */
        $form = new formpresenter($category->arrLabels, $category->arrFormElms, $arrValues);
        echo $form->presentForm();

        sysFooter();
        break;

    case "SAVE":
        $category = new shopcategory();


        /* Save method*/
        $iCategoryID = $category->save();
        header("Location: ?mode=details&iCategoryID=" . $iCategoryID);
        break;

    case "DELETE":
        $category = new shopcategory();
        $category->iCategoryID = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);
        $category->delete($iCategoryID);
        header("Location: ?mode=list");
        break;


}

require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/footer.php"; ?>
<script src="/assets/js/ajaxFunctions.js"></script>
