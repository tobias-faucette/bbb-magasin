<?php require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";
?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php projectName() ?></title>
    <link rel="icon" type="image/png" href="../images/favicon-cog.ico">
    <meta name="viewport" content="width=device-width" initial-scale="1"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="/cms/assets/css/main.css" rel="stylesheet" type="text/css"/>
    <link href="/cms/assets/css/summernote.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link href='https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css' rel='stylesheet'
          type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Montserrat|Source+Sans+Pro|Rock Salt'
          rel='stylesheet' type='text/css'>


</head>
<body>
<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand logo" href="#"><i class="fa fa-globe"></i><?php projectName() ?></a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="fa fa-user"></i> <?php echo get_current_user() ?> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="../admin/profile.php"><i class="fa fa-fw fa-user"></i> Profile</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="?action=logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li>
                    <a href="../modules/event.php?mode=list"><i class="fa fa-fw fa-calendar-o"></i> Events</a>
                </li>
                <li>
                    <a href="../modules/news.php?mode=list"><i class="fa fa-fw fa-newspaper-o"></i> News</a>
                </li>
                <li>
                    <a href="../modules/category.php?mode=list"><i class="fa fa-fw fa-list-ol"></i> Categories</a>
                </li>
                <li>
                    <a href="../modules/comment.php?mode=list"><i class="fa fa-fw fa-comments-o"></i> Comments</a>
                </li>
                <li>
                    <a href="../admin/org.php?mode=list"><i class="fa fa-fw fa-sitemap"></i> Organization</a>
                </li>
                <li>
                    <a href="../admin/user.php?mode=list"><i class="fa fa-fw fa-user-plus"></i> Users</a>
                </li>
                <li>
                    <a href="../admin/usergroup.php?mode=list"><i class="fa fa-fw fa-users"></i> Usergroups</a>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false"><i class="fa fa-fw fa-shopping-basket"></i> Shop <span
                                class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="../modules/shopproduct.php?mode=list"><i class="fa fa-fw fa-truck"></i>
                                ShopProduct</a>
                        </li>
                        <li>
                            <a href="../modules/shopcategory.php?mode=list"><i class="fa fa-fw fa-shopping-basket"></i>
                                ShopCategory</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="../modules/fileupload.php"><i class="fa fa-fw fa fa-upload"></i>
                        File Upload</a>
                </li>
                <li>
                    <a href="../modules/newsletter.php"><i class="fa fa-fw fa fa-envelope-o"></i>
                        Newsletter</a>
                </li>
                <li>
                    <a href="../admin/config.php?mode=list"><i class="fa fa-fw fa-cog"></i> Config</a>
                </li>

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
