<?php
/**
 * header include function
 */
function sysHeader() {
    global $db;
    require_once DOCROOT . '/cms/assets/incl/header.php';
}

/**
 * footer include function
 */
function sysFooter() {
    global $db;
    require_once DOCROOT . '/cms/assets/incl/footer.php';
}

/**
 * Displays a read friendly var_dump
 * @param array $array
 * @param int $view
 */
function showme($array, $view = 0) {
    print ($view > 0) ? "<xmp>\n" : "<pre>\n";
    highlight_string("<?php\n\$data =\n" . var_export($array, true) . ";\n?>");
    var_dump($array);
    print ($view > 0) ? "\n</xmp>" : "\n</pre>";
}

/**
 * Get project name
 * Primary use for page title,footer & brand text.
 */
function projectName() {
    echo basename(dirname(__DIR__));
}

/**
 * Gets page mode from GET or POST - otherwise return default
 * @param string $default
 * @return string Returns selected string
 */
function setMode($default = "list") {
    $mode = filter_input(INPUT_POST, "mode", FILTER_SANITIZE_STRING);
    if (empty($mode)) {
        $mode = filter_input(INPUT_GET, "mode", FILTER_SANITIZE_STRING);
    }
    if (empty($mode)) {
        $mode = $default;
    }
    return $mode;
}


//function getIcon($strIcon, $strUrl = "") {
//    $strHtml = "";
//    if (!empty($strUrl)) {
//        $strHtml .= '<a href=' . $strUrl . '>';
//    }
//    $strHtml .= "<i class=\"fa fa-" . $strIcon . "\"></i>";
//    if (!empty($strUrl)) {
//        $strHtml .= "</a>";
//    }
//    return $strHtml;
//}

/**
 * Writes an icon with optional settings
 * @param $strUrl string $strUrl URL for the icon reference
 * @param $strIcon string $strIcon Icon name according to font awesome archive. The "fa-" is prefixed.
 * @param string $strTitle [Optional] Content of title tag for the icon.
 * @param string $strScript [Optional] Click Event - disables the URL.
 * @param string $strClass [Optional] Adds an additional class to the span tag.
 * @return string Returns a string with accumlated html.
 */
function getIcon($strUrl, $strIcon, $strTitle = "", $strScript = "", $strClass = "") {
    $attrClass = (!empty($strClass)) ? $strClass : "icon";
    $attrEvent = (!empty($strScript)) ? "onclick=\"" . $strScript . "\"" : "";
    $attrHref = (!empty($strUrl) && empty($strScript)) ? $strUrl : "Javascript:void(0)";

    $strHtml = "<a href=\"" . $attrHref . "\"" . $attrEvent . ">\n";
    $strHtml .= "<span class=\"" . $attrClass . " fa fa-" . $strIcon . "\" title=\"" . $strTitle . "\"></span>\n";
    if (!empty($strUrl) || !empty($strScript)) {
        $strHtml .= "</a>\n";
    }
    return $strHtml;
}


// Returns a html string with a button like link
function getButtonLink($strIcon, $strUrl = "#", $strText = "Untitled", $strClass = "btn-default") {
    $strHtml = "<a href=\"" . $strUrl . "\" class=\"btn " . $strClass . "\"><i class=\"fa fa-" . $strIcon . "\"></i>  $strText  </a>\n";
    return $strHtml;
}


/**
 * Returns a html string with the button element
 * @param $type
 * @param $value
 * @param string $event
 * @param string $class
 * @param string $data
 * @return string
 */
function getButton($type, $value, $event = "", $class = "btn-default", $data = "") {
    $event = !empty($event) ? " onclick=\"" . $event . "\"" : "";
    $data = !empty($data) ? $data : "";
    $strHtml = "<button type=\"" . $type . "\" class=\"btn " . $class . "\"" . $event . "" . $data . ">" . $value . "</button>\n";
    return $strHtml;
}


/**
 * @param $string
 * @return mixed
 */

// Cleans a string for alphas and symbols
function onlyLetters($string) {
    return preg_replace("/[^a-zA-Z]+$/", "", $string);
}


/**
 * Returns a date/time string with locale month and day names
 * @param int $iStamp A timestamp
 * @param bool $iUseHours Use hours and minutes if set to true
 * @return string Returns a formatted date string
 */
function date2local($iStamp, $iUseHours = 0) {
    $arrDays = array(1 => "Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "Lørdag", "Søndag");
    $arrMonths = array(1 => "Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December");
    $dateFormat = date("j", $iStamp) . ". " . $arrMonths[date("n", $iStamp)] . " " . date("Y", $iStamp);
    if ($iUseHours) {
        $dateFormat .= " " . date("k\l. H:i", $iStamp);
    }
    return $dateFormat;
}

/**
 * Returns a friendly date & time format according to locale settings
 * @param int $iStamp
 * @return string Returns a formatted date string
 */
function time2local($iStamp) {
    return date2local($iStamp, 1);
}


// * Builds a select box with name, options and selected value
// * Optional settings for multiple and event features
// * @param string $strName Name/Id attribute on selectbox
// * @param array $arrOptions Multi-dimensional array of options
// *          (key = numeric, value = array(option-value, option-text))
// * @param var $value Value to be selected (Optional)
// * @param bool $isMultiple Set to true if multple select
// * @param string $strOnChangeEvent Set onchange event call if needed (Ex: doSomething())
// * @return string Returns a html string with a select box

function SelectBox($strName, $arrOptions, $value = NULL, $isMultiple = FALSE, $strOnChangeEvent = '') {
    /* Set multiple attribute if argument is true */
    $strMultiple = ($isMultiple === TRUE) ? "multiple" : "";
    /* Set name to array mode if multiple argument is true */
    $strAttrName = ($isMultiple === TRUE) ? $strName . "[]" : $strName;
    /* Set name of onchange event if argument not empty */
    $strEvent = (!empty($strOnChangeEvent)) ? "onchange=\"" . $strOnChangeEvent . "\"" : "";
    /* Build select tag with settings */
    $strHtml = "<select class=\"form-control\" id=\"" . $strName . "\" " .
        "name=\"" . $strAttrName . "\"" . $strMultiple . "" . $strEvent . ">\n";
    /* Loop through arrOptions */
    foreach ($arrOptions as $arrOptionInfo) {
        /* Redefine option keys to numbers */
        $arrOptionInfo = array_values($arrOptionInfo);
        /* Set selected state on multiple or single selectbox */
        $selected = ($isMultiple === TRUE) ?
            in_array($arrOptionInfo[0], $value) ? "selected" : "" :
            ($value === $arrOptionInfo[0]) ? "selected" : "";
        /* Add option tag to strHtml */
        $strHtml .= "<option value=\"" . $arrOptionInfo[0] . "\" " . $selected . ">" . $arrOptionInfo[1] . "</option>\n";
    }
    /* Close select tag */
    $strHtml .= "</select>\n";
    /* Return strHtml */
    return $strHtml;
}


/**
 * @param $strElm
 * @return false|int
 */

// Creates a timestamp from form date select values
function makeStamp($strElm) {
    $arrFormats = array("day", "month", "year", "hours", "minutes");
    $arrDate = array();

    foreach ($arrFormats as $value) {
        $arrDate[$value] = filter_input(INPUT_POST, $strElm . "_" . $value, FILTER_SANITIZE_NUMBER_INT, getDefaultValue(0));
    }
    return mktime($arrDate["hours"], $arrDate["minutes"], 0, $arrDate["month"], $arrDate["day"], $arrDate["year"]);
}


/**
 * @param $val
 * @return string
 */
// Converts a bool value to FA check/ban icon
function boolToIcon($val) {
    $strIcon = ($val > 0) ? "check" : "ban";
    return "<span class=\"testing fa fa-" . $strIcon . "\"></span\n";
}


/**
 * @param $val
 * @return string
 */

// Converts a bool value to string
function boolToText($val) {
    return ($val > 0) ? "Ja" : "Nej";
}

/**
 * @param $val
 * @return array
 */
// Set Default Value for filter_inputs
function getDefaultValue($val) {
    $options = array("options" => array("default" => $val));
    return $options;
}


/**
 * @param $val
 * @param $array
 * @return mixed
 */
function boolToCustom($val, $array) {
    return $array[$val];
}


/**
 * Formats a price to local
 * @param float $val
 * @return float formatted price
 */

function formatPrice($val, $friendly = 1) {
    if ($friendly > 0) {
        return number_format($val, 2, ",", ".");
    } else {
        return number_format($val, 2, ".");
    }
}

/**
 * @param $str
 * @return mixed
 */
// Get table field abbreviation
function getAbbr($str) {
    $Abbr = preg_split('/(?=[A-Z])/', $str);
    return reset($Abbr);
}


/**
 * getImage function
 * @return string
 */

function getImage() {
    $dirname = "images/gallery/";
    $images = glob($dirname . "*.{jpg,gif,png}", GLOB_BRACE);
    $accHtml = "";

    foreach ($images as $image) {
        $accHtml .= "<figure class=\"col-md-3 image-style\">\n\t\t";
        $accHtml .= "<img src=\"" . $image . "\" class=\"img-responsive\" alt=\"Photo_gallery\" draggable=\"false\"/>\n\t";
        $accHtml .= "</figure>\n\t";

    }
    return $accHtml;
}


// Sets the text to bold on navbar if selected
function echoActiveClassIfRequestMatches($requestUri) {
    $current_file_name = basename($_SERVER['REQUEST_URI'], ".php");

    if ($current_file_name == $requestUri)
        echo 'class="active"';
}


function newArrivals() {
    $product = new shopproduct();
    $rows = $product->arrivals();

    foreach ($rows as $key => $row) {


    }
}

/**
 * Function to display copyright
 * Use of date function & HTML
 */
function copyright() {
    echo "<p>" . date("Y") . '&copy; tobi cms' . "</p>";
}


/**
 * Front-end orientated copyright
 *
 */

function copyrightFirm() {
    echo "<p>" . "Copyright &copy;" . " " . date("Y") . ' All Rights Reserved City' . "  " . "<strong>Fashion Online</strong>" . "</p>";
}
