<?php

/**
 * Created City PhpStorm.
 * usergroup: tn116
 * Date: 17-05-2017
 * Time: 09:21
 */
class usergroup
{
    public $db;
    public $iGroupID;
    public $vcGroupName;
    public $vcRoleName;
    public $txDesc;
    public $daCreated;
    public $iDeleted;

    public function __construct() {
        global $db;
        $this->db = $db;


        $this->arrLabels = array(
            "iGroupID" => "ID",
            "vcGroupName" => "Gruppe Navn",
            "vcRoleName" => "Rolle Navn",
            "txDesc" => "Description",
            "daCreated" => "Oprettet",


        );

        /**
         * Array for formfields:
         * Index = fieldname
         * Value[0] = formtype
         * Value[1] = filter_type
         * Value[2] = Required Status (TRUE/FALSE)
         * Value[3] = Default value
         */
        $this->arrFormElms = array(
            "iGroupID" => array("hidden", FILTER_VALIDATE_INT, FALSE, 0),
            "vcGroupName" => array("text", FILTER_SANITIZE_STRING, TRUE, "hej"),
            "vcRoleName" => array("text", FILTER_SANITIZE_STRING, FALSE, ""),
            "txDesc" => array("textarea", FILTER_SANITIZE_STRING, FALSE, ""),
            "daCreated" => array("hidden", FILTER_VALIDATE_INT, FALSE, "")


        );

        $this->arrValues = array();
    }

    /**
     * function to get list of usergroupsR
     * @return array
     */

    public function getlist() {  //function = method
        $sql = "SELECT * FROM usergroup WHERE iDeleted = 0";

        return $this->db->_fetch_array($sql); //gets all rows, fetch value will take out a single row.

    }


    /**
     * function to get a single Event
     * @param $iGroupID
     * @return array
     */
    public function getGroup($iGroupID) { //set parameter iGroupID to get a single Event
        $this->iGroupID = $iGroupID;
        $sql = "SELECT * FROM usergroup WHERE iGroupID = ? AND iDeleted = 0";
        $row = $this->db->_fetch_array($sql, array($this->iGroupID));
        foreach ($row[0] as $key => $value) {
            $this->$key = $value;
        }

        return $row;
        //showme($row);
    }


    /**
     * @return int
     *
     */
    public function save() {
        if ($this->iGroupID > 0) {
            //UPDATE MODE
            $params = array(
                $this->vcGroupName,
                $this->vcRoleName,
                $this->txDesc,
                $this->daCreated,
                $this->iGroupID
            );

            $sql = "UPDATE usergroup SET " .
                "vcGroupName = ?, " .
                "vcRoleName = ?, " .
                "txDesc = ?, " .
                "daCreated = ? " .
                "WHERE iGroupID = ? ";

            $this->db->_query($sql, $params);
            return $this->iGroupID;


        } else {
            //CREATE MODE
            $params = array(
                $this->vcGroupName,
                $this->vcRoleName,
                $this->txDesc,
                time()

            );

            $sql = "INSERT INTO usergroup (" .
                "vcGroupName, " .
                "vcRoleName, " .
                "txDesc, " .
                "daCreated) " .
                "VALUES(?,?,?,?)";
            showme($_POST);
            //exit();
            $this->db->_query($sql, $params);

            return $this->db->_getinsertid();

        }

    }

    public function delete() {
        $params = array($this->iGroupID);

        $sql = "UPDATE usergroup set " .
            "iDeleted = 1 " .
            "WHERE iGroupID = ? ";
        $this->db->_query($sql, $params);

    }

}

