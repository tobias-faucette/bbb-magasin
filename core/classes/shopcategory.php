<?php

class shopcategory
{
    public $db;
    public $iCategoryID;
    public $iParentID;
    public $vcTitle;
    public $txDescription;
    public $vcImage;
    public $daCreated;
    public $iIsActive;
    public $iSortNum;
    public $iDeleted;


    public function __construct() {
        global $db;
        $this->db = $db;


        $this->arrLabels = array(
            "iCategoryID" => "ID",
            "iParentID" => "Kategori",
            "vcTitle" => "Produkt Navn",
            "txDescription" => "Short Description",
            "vcImage" => "Billede",
            "daCreated" => "Oprettet",
            "iIsActive" => "Active",
            "iSortNum" => "Sortering",

        );

        /**
         * Array for formfields:
         * Index = fieldname
         * Value[0] = formtype
         * Value[1] = filter_type
         * Value[2] = Required Status (TRUE/FALSE)
         * Value[3] = Default value
         */
        $this->arrFormElms = array(
            "iCategoryID" => array("hidden", FILTER_VALIDATE_INT, FALSE, 0),
            "iParentID" => array("select", FILTER_SANITIZE_STRING, FALSE, ""),
            "vcTitle" => array("text", FILTER_SANITIZE_STRING, TRUE, ""),
            "txDescription" => array("textarea", FILTER_SANITIZE_STRING, TRUE, ""),
            "vcImage" => array("text", FILTER_SANITIZE_STRING, FALSE, ""),
            "daCreated" => array("hidden", FILTER_VALIDATE_INT, FALSE, ""),
            "iIsActive" => array("number", FILTER_VALIDATE_INT, FALSE, ""),
            "iSortNum" => array("number", FILTER_VALIDATE_INT, FALSE, ""),


        );

        $this->arrValues = array();
    }


    public function listOptions() {
        $strSelect = "SELECT iCategoryID, vcTitle " .
            "FROM shopcategory " .
            "WHERE iDeleted = 0";
        return $this->db->_fetch_array($strSelect);
    }


    /**
     * function to get list of products
     * @return array
     */

    public function getlist() {  //function = method
        $sql = "SELECT * FROM shopcategory WHERE iDeleted = 0";

        return $this->db->_fetch_array($sql); //gets all rows, fetch value will take out a single row.

    }

    /**
     * function to get a single Event
     * @param $iCategoryID
     */
    public function getCategory($iCategoryID) { //set parameter iEventID to get a single Event
        $this->iCategoryID = $iCategoryID;
        $sql = "SELECT * FROM shopcategory WHERE iCategoryID = ? AND iDeleted = 0";
        $row = $this->db->_fetch_array($sql, array($this->iCategoryID));
        foreach ($row[0] as $key => $value) {
            $this->$key = $value;
        }
        //showme($row);
    }


    /**
     * Get product related groups
     * @return array
     */

    public function getGroupRelations() {
        $params = array($this->arrValues["iCategoryID"]);
        $strSelect = "SELECT c.iCategoryID, c.vcTitle " .
            " FROM shopcategory c " .
            "JOIN shopcatprodrel x ON x.iCategoryID = c.iCategoryID " .
            "WHERE x.iCategoryID = ? " .
            "AND c.iDeleted = 0";
        return $this->db->_fetch_array($strSelect, $params);
    }


    /**
     * Save item
     */
    public function save() {
        $iCategoryID = parent::saveItem();

        /* Remove all group relations for the product */
        $params = array($iCategoryID);
        $strDelete = "DELETE FROM shopcatprodrel WHERE iCategoryID = ?";
        $this->db->_query($strDelete, $params);

        /* Create arguments for post filtering */
        $args = array(
            "arrProductGroups" => array(
                "filter" => FILTER_VALIDATE_INT,
                "flags" => FILTER_REQUIRE_ARRAY
            )
        );
        $arrInputVal = filter_input_array(INPUT_POST, $args);

        /* Save user related groups if any */
        if (count($arrInputVal["arrProductGroups"])) {
            $arrValues = array_values($arrInputVal["arrProductGroups"]);
            foreach ($arrValues as $value) {
                $params = array($iCategoryID, $value);
                $strInsert = "INSERT INTO shopcatprodrel(iCategoryID, iCategoryID) VALUES(?,?)";
                $this->db->_query($strInsert, $params);
            }
        }
        return $iCategoryID;
    }

    /**
     * Delete item
     */

    public function delete($iItemID) {
        parent::delete($iItemID);
    }


}