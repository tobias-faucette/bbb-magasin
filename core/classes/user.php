<?php

/**
 * Created City PhpStorm.
 * User: tn116
 * Date: 17-05-2017
 * Time: 09:21
 */
class user
{
    public $db;
    public $iUserID;
    public $vcImage;
    public $vcUserName;
    public $vcPassword;
    public $vcFirstName;
    public $vcLastName;
    public $vcAddress;
    public $iZip;
    public $vcCity;
    public $vcEmail;
    public $vcPhone1;
    public $vcPhone2;
    public $iOrgID;
    public $daCreated;
    public $iSuspended;
    public $iDeleted;

    public $arrLabels;
    public $arrFormElms;
    public $arrValues;
    public $arrGroups;

    public $sysadmin;
    public $admin;
    public $extranet;
    public $newsletter;

    public function __construct() {
        global $db;
        $this->db = $db;
        //$this->createtable();

        $this->arrLabels = array(
            "iUserID" => "ID",
            "vcUserName" => "Brugernavn",
            "vcPassword" => "Adgangskode",
            "vcFirstName" => "Fornavn",
            "vcLastName" => "Efternavn",
            "vcAddress" => "Adresse",
            "iZip" => "Postnummer",
            "vcCity" => "City",
            "vcEmail" => "Email",
            "vcPhone1" => "Telefon",
            "vcPhone2" => "Mobil",
            "iOrgID" => "Organization",
            "daCreated" => "Oprettelse",
            "iSuspended" => "Suspenderet"


        );

        /**
         * Array for formfields:
         * Index = fieldname
         * Value[0] = formtype
         * Value[1] = filter_type
         * Value[2] = Required Status (TRUE/FALSE)
         * Value[3] = Default value
         */
        $this->arrFormElms = array(
            "iUserID" => array("hidden", FILTER_VALIDATE_INT, FALSE, 0),
            "vcUserName" => array("text", FILTER_SANITIZE_STRING, TRUE, ""),
            "vcPassword" => array("password", FILTER_SANITIZE_STRING, TRUE, ""),
            "vcFirstName" => array("text", FILTER_SANITIZE_STRING, TRUE, ""),
            "vcLastName" => array("text", FILTER_SANITIZE_STRING, TRUE, ""),
            "vcAddress" => array("text", FILTER_SANITIZE_STRING, FALSE, ""),
            "iZip" => array("number", FILTER_VALIDATE_INT, FALSE, ""),
            "vcCity" => array("text", FILTER_SANITIZE_STRING, FALSE, ""),
            "vcEmail" => array("email", FILTER_SANITIZE_STRING, TRUE, ""),
            "vcPhone1" => array("number", FILTER_VALIDATE_INT, FALSE, ""),
            "vcPhone2" => array("number", FILTER_VALIDATE_INT, FALSE, ""),
            "iOrgID" => array("select", FILTER_VALIDATE_INT, FALSE, ""),
            "daCreated" => array("hidden", FILTER_VALIDATE_INT, FALSE, ""),
            "iSuspended" => array("number", FILTER_VALIDATE_INT, FALSE, "")

        );

        $this->arrValues = array();

    }

    /**
     * function to get list of users
     * @return array
     */

    public function getlist() {  //function = method
        $sql = "SELECT * FROM user WHERE iDeleted = 0";

        return $this->db->_fetch_array($sql); //gets all rows, fetch value will take out a single row.

    }


    /**
     * function to get a single user
     * @param $iUserID
     */
    /*    public function getuser($iUserID) { //set parameter iUserID to get a single user
            $this->iUserID = $iUserID;
            $sql = "SELECT * FROM user WHERE iUserID = ? AND iDeleted = 0";
            $row = $this->db->_fetch_array($sql, array($this->iUserID));
            foreach ($row[0] as $key => $value) {
                $this->$key = $value;
            }
            //showme($row);
        }*/

    /**
     * Class Method GetUser
     * @param int $iUserID
     * Selects City id and add values to class properties
     * Used City Auth initUser to get group relations on login
     */
    public function getuser($iUserID) {
        $this->iUserID = $iUserID;
        $sql = "SELECT u.*, o.vcOrgName " .
            "FROM user u " .
            "LEFT JOIN org o " .
            "ON u.iOrgID = o.iOrgID " .
            "WHERE iUserID = ? " .
            "AND u.iDeleted = 0";
        if ($row = $this->db->_fetch_array($sql, array($this->iUserID))) {
            foreach ($row[0] as $key => $value) {
                $this->$key = $value;
            }
            $this->arrGroups = $this->getgrouprelations();

            foreach ($this->arrGroups as $value) {
                $role = strtolower($value["vcRoleName"]);
                $this->$role = 1;
            }
        }
    }


    /**
     * Return a securely salted SHA256 hash of the entered password
     *
     * @param string $password
     *
     * @return string
     */
    public function hashPassword( $vcPassword ) {
        $salt = "FOLfqubh4WjsI2PNnNT7mde1BbVQflMjEowWSSoiZT2mjS4UzC8LuJ0znMUTGnr3mggYWFEJlHns74uvAzZgS8lTFcZjYuDfMD9Hl3NX3gERGgNFCZirF9bIwj9CPdQV";
        for ( $i = 0; $i < 10; $i ++ ) {
            $vcPassword = str_rot13( strrev( hash( "sha256", $vcPassword ) ) ) . $salt;
        }

        $vcPassword = hash( "sha256", str_rot13( strrev( strrev( $vcPassword ) . $salt ) ) );

        return $vcPassword;

        $checkPassword = $this["vcPassword"] == hash("sha256", $vcPassword . HASHING_STRING);
    }


    public function checkPassword(){

    }
//    /**
//     * if user & usersession table does not exist in database, create them.
//     */
//    public function createtable() {
//        $sql = "CREATE TABLE IF NOT EXISTS `user` (
//	`iUserID` BIGINT(20) NOT NULL AUTO_INCREMENT,
//	`vcUserName` VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'Brugernavn',
//	`vcPassword` VARCHAR(50) NOT NULL DEFAULT '' COMMENT 'Adgangskode',
//	`vcFirstName` VARCHAR(255) NULL DEFAULT '' COMMENT 'Fornavn',
//	`vcLastName` VARCHAR(255) NULL DEFAULT '' COMMENT 'Efternavn',
//	`vcAddress` VARCHAR(255) NULL DEFAULT '' COMMENT 'Adresse',
//	`iZip` MEDIUMINT(10) NULL DEFAULT NULL COMMENT 'Postnummer',
//	`vcCity` VARCHAR(255) NULL DEFAULT '' COMMENT 'City',
//	`vcEmail` VARCHAR(255) NULL DEFAULT '' COMMENT 'Email',
//	`vcPhone1` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Tlf1',
//	`vcPhone2` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Tlf2',
//	`iOrgID` BIGINT(20) NULL DEFAULT NULL COMMENT 'Organisation',
//	`daCreated` BIGINT(20) NULL DEFAULT NULL COMMENT 'Oprettelsesdato',
//	`iSuspended` TINYINT(4) NOT NULL DEFAULT '0' COMMENT 'Suspenderet',
//	`iDeleted` TINYINT(4) NOT NULL DEFAULT '0',
//	PRIMARY KEY (`iUserID`)
//)
//    COLLATE='utf8_general_ci'
//    ENGINE=InnoDB
//    AUTO_INCREMENT=10
//    ;";
//        $this->db->_query($sql);
//
//
//        $sql = "CREATE TABLE IF NOT EXISTS `usersession` (
//	`vcSessionID` VARCHAR(32) NOT NULL DEFAULT '',
//	`iUserID` BIGINT(20) NOT NULL DEFAULT '0',
//	`iIpAddress` VARCHAR(24) NOT NULL DEFAULT '',
//	`iIsLoggedIn` TINYINT(1) NOT NULL DEFAULT '0',
//	`daLoginCreated` BIGINT(20) NOT NULL DEFAULT '0',
//	`daLastAction` BIGINT(20) NOT NULL DEFAULT '0'
//)
//    COLLATE='utf8_general_ci'
//    ENGINE=InnoDB
//    ;";
//
//        $this->db->_query($sql);
//    }


    /**
     * @return int
     *
     */
    public function save() {
        if ($this->iUserID > 0) {
            //UPDATE MODE
            $params = array(
                $this->vcUserName,
                $this->hashPassword($this->vcPassword),
                $this->vcFirstName,
                $this->vcLastName,
                $this->vcAddress,
                $this->iZip,
                $this->vcCity,
                $this->vcEmail,
                $this->vcPhone1,
                $this->vcPhone2,
                $this->iOrgID,
                $this->daCreated,
                $this->iSuspended,
                $this->iUserID
            );

            $sql = "UPDATE user SET " .
                "vcUserName = ?, " .
                "vcPassword = ?, " .
                "vcFirstName = ?, " .
                "vcLastName = ?, " .
                "vcAddress = ?, " .
                "iZip = ?, " .
                "vcCity = ?, " .
                "vcEmail = ?, " .
                "vcPhone1 = ?, " .
                "vcPhone2 = ?, " .
                "iOrgID = ?, " .
                "daCreated = ?, " .
                "iSuspended = ? " .
                "WHERE iUserID = ? ";

            $this->db->_query($sql, $params);
            return $this->iUserID;
        } else {
            //CREATE MODE
            $params = array(
                $this->vcUserName,
                $this->hashPassword($this->vcPassword),
                $this->vcFirstName,
                $this->vcLastName,
                $this->vcAddress,
                $this->iZip,
                $this->vcCity,
                $this->vcEmail,
                $this->vcPhone1,
                $this->vcPhone2,
                $this->iOrgID,
                time(),
                $this->iSuspended
            );

            echo $sql = "INSERT INTO user (" .
                "vcUserName, " .
                "vcPassword, " .
                "vcFirstName, " .
                "vcLastName, " .
                "vcAddress, " .
                "iZip, " .
                "vcCity, " .
                "vcEmail, " .
                "vcPhone1, " .
                "vcPhone2, " .
                "iOrgID, " .
                "daCreated, " .
                "iSuspended) " .
                "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
            /*            showme($_POST);
                        exit();*/
            $this->db->_query($sql, $params);

            return $this->db->_getinsertid();

        }

    }

    /**
     * Get user related groups
     * @return array of related groups
     */
    public function getgrouprelations() {
        $params = array($this->iUserID);
        $strSelect = "SELECT g.iGroupID, g.vcGroupName, g.vcRoleName " .
            "FROM usergroup g " .
            "LEFT JOIN usergrouprel x " .
            "ON x.iGroupID = g.iGroupID " .
            "WHERE x.iUserID = ? " .
            "AND g.iDeleted = 0";
        return $this->db->_fetch_array($strSelect, $params);
    }


    public function delete() {
        $params = array($this->iUserID);

        $sql = "UPDATE user SET " .
            "iDeleted = 1 " .
            "WHERE iUserID = ? ";
        $this->db->_query($sql, $params);

    }

}

