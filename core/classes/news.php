<?php
/**
 * Created by PhpStorm.
 * User: tn116
 * Date: 28-08-2017
 * Time: 08:31
 */

class news
{
    public $db;
    public $iNewsID;
    public $iCatID;
    public $vcTitle;
    public $daCreated;
    public $txContent;
    public $iIsActive;
    public $iDeleted;


    public function __construct() {
        global $db;
        $this->db = $db;


        $this->arrLabels = array(
            "iNewsID" => "ID",
            "iCatID" => "Category",
            "vcTitle" => "Article Name",
            "daCreated" => "Created",
            "txContent" => "Short Description",
            "iIsActive" => "Active",

        );

        /**
         * Array for formfields:
         * Index = fieldname
         * Value[0] = formtype
         * Value[1] = filter_type
         * Value[2] = Required Status (TRUE/FALSE)
         * Value[3] = Default value
         */
        $this->arrFormElms = array(
            "iNewsID" => array("hidden", FILTER_VALIDATE_INT, FALSE, 0),
            "iCatID" => array("select", FILTER_VALIDATE_INT, FALSE, 0),
            "vcTitle" => array("text", FILTER_SANITIZE_STRING, TRUE, "h"),
            "daCreated" => array("hidden", FILTER_VALIDATE_INT, FALSE, ""),
            "txContent" => array("textEdit", FILTER_SANITIZE_STRING, TRUE, ""),
            "iIsActive" => array("number", FILTER_VALIDATE_INT, FALSE, ""),


        );

        $this->arrValues = array();
    }


    /**
     * function to get list of news
     * @return array
     */

    public function getlist() {  //function = method
        $sql = "SELECT * FROM news WHERE iDeleted = 0";

        return $this->db->_fetch_array($sql); //gets all rows, fetch value will take out a single row.

    }

    public function getAllNews($Limit) {
        $sql = "SELECT * FROM news WHERE iDeleted = 0 ORDER BY daCreated ASC LIMIT " . $Limit;
        return $this->db->_fetch_array($sql);

    }


    public function getPopulareNews($Limit) {
        $sql = "SELECT vcTitle FROM news WHERE iDeleted = 0 ORDER BY iViews DESC LIMIT " . $Limit;
        return $this->db->_fetch_array($sql);

    }


    /**
     * function to get a single Event
     * @param $iNewsID
     * @return array
     */
    public function getNews($iNewsID) { //set parameter iNewsID to get a single Event
        $this->iNewsID = $iNewsID;
        $sql = "SELECT * FROM news WHERE iNewsID = ? AND iDeleted = 0";
        $row = $this->db->_fetch_array($sql, array($this->iNewsID));
        foreach ($row[0] as $key => $value) {
            $this->$key = $value;
        }

        return $row;
        //showme($row);
    }


    /**
     * Save item
     */
    public function save() {
        if ($this->iNewsID > 0) {
            //UPDATE MODE
            $params = array(
                $this->iCatID,
                $this->vcTitle,
                $this->daCreated,
                $this->txContent,
                $this->iIsActive,
                $this->iNewsID
            );

            $sql = "UPDATE news SET " .
                "iCatID = ?, " .
                "vcTitle = ?, " .
                "daCreated = ?, " .
                "txContent = ?, " .
                "iIsActive = ? " .
                "WHERE iNewsID = ? ";

            $this->db->_query($sql, $params);
            return $this->iNewsID;

        } else {
            //CREATE MODE
            $params = array(
                $this->iCatID,
                $this->vcTitle,
                time(),
                $this->txContent,
                $this->iIsActive,
            );

            $sql = "INSERT INTO news (" .
                "iCatID, " .
                "vcTitle, " .
                "daCreated, " .
                "txContent, " .
                "iIsActive) " .
                "VALUES(?,?,?,?,?)";
            $this->db->_query($sql, $params);

            return $this->db->_getinsertid();

        }

    }

    /**
     * Delete item
     */

    public function delete() {
        $params = array($this->iNewsID);

        $sql = "UPDATE news SET " .
            "iDeleted = 1 " .
            "WHERE iNewsID = ? ";
        $this->db->_query($sql, $params);
    }
}