<?php
/**
 * Created by PhpStorm.
 * User: tn116
 * Date: 31-08-2017
 * Time: 22:13
 */

class comment
{
    public $db;
    public $iCommentID;
    public $vcTitle;
    public $vcName;

    public $daCreated;

    public $iDeleted;

    public function __construct() {
        global $db;
        $this->db = $db;


        $this->arrLabels = array(
            "iCommentID" => "ID",
            "vcTitle" => "Article Name",
            "daCreated" => "Created",
            "txContent" => "Short Description",
            "vcTitle" => "Active",

        );

        /**
         * Array for formfields:
         * Index = fieldname
         * Value[0] = formtype
         * Value[1] = filter_type
         * Value[2] = Required Status (TRUE/FALSE)
         * Value[3] = Default value
         */
        $this->arrFormElms = array(
            "iCommentID" => array("hidden", FILTER_VALIDATE_INT, FALSE, 0),
            "vcTitle" => array("text", FILTER_SANITIZE_STRING, TRUE, ""),
            "iNewsID" => array("hidden", FILTER_VALIDATE_INT, FALSE, "0"),
            "daCreated" => array("hidden", FILTER_VALIDATE_INT, FALSE, ""),
            "txContent" => array("textEdit", FILTER_SANITIZE_STRING, TRUE, ""),
            "vcName" => array("text", FILTER_SANITIZE_STRING, TRUE, ""),


        );

        $this->arrValues = array();
    }


    /**
     * function to get list of comment
     * @return array
     */

    public function getlist() {  //function = method
        $sql = "SELECT * FROM comment WHERE iDeleted = 0";

        return $this->db->_fetch_array($sql); //gets all rows, fetch value will take out a single row.

    }


    /**
     * function to get a single record
     * @param $iCommentID
     * @return array
     */
    public function getComment($iCommentID) { //set parameter iCommentID to get a single record
        $this->iCommentID = $iCommentID;
        $sql = "SELECT *
        FROM comment
        WHERE iCommentID = ?
        AND iDeleted = 0";
        $row = $this->db->_fetch_array($sql, array($this->iCommentID));
        foreach ($row[0] as $key => $value) {
            $this->$key = $value;
        }

        return $row;
        //showme($row);
    }

    /**
     * @param $iCommentID
     * @return array
     */
    public function newsComment($iCommentID) {
        $this->iCommentID = $iCommentID;
        $sql = "SELECT c.*, n.iNewsID " .
            "FROM comment c " .
            "LEFT JOIN news n " .
            "ON c.iNewsID = n.iNewsID " .
            "WHERE c.iNewsID = ? " .
            "AND c.iDeleted = 0 " .
            "ORDER BY c.iCommentID ASC";
        if ($row = $this->db->_fetch_array($sql, array($this->iCommentID))) {
            foreach ($row[0] as $key => $value) {
                $this->$key = $value;
            }

            return $row;
        }
    }

//    public function countComment($iNewsID) {
//        $this->iNewsID = $iNewsID;
//        $sql = "SELECT count(*) " .
//            "FROM comment c " .
//            "LEFT JOIN news n " .
//            "ON c.iNewsID = n.iNewsID " .
//            "WHERE c.iNewsID = ? " .
//            "AND c.iDeleted = 0";
//        if ($row = $this->db->_fetch_array($sql, array($this->iNewsID))) {
//            foreach ($row[0] as $key => $value) {
//                $this->$key = $value;
//            }
//
//            return $row;
//
//        }
//
//    }

    /**
     * Count comments per article
     * @param $iNewsID
     * @return mixed
     *
     */
    public function commentCount($iNewsID) {
        $sql = "SELECT COUNT(*) FROM comment WHERE iNewsID = " . (int)$iNewsID . " ";

        $result = $this->db->_fetch_array($sql);

        return $result[0]["COUNT(*)"];
    }

    /**
     * Save item
     */
    public
    function save() {
        if ($this->iCommentID > 0) {
            //UPDATE MODE
            $params = array(
                $this->vcTitle,
                $this->daCreated,
                $this->txContent,
                $this->vcTitle,
            );

            $sql = "UPDATE comment SET " .
                "vcTitle = ?, " .
                "daCreated = ?, " .
                "txContent = ?, " .
                "vcTitle = ?," .
                "WHERE iCommentID = ? ";

            $this->db->_query($sql, $params);
            return $this->iCommentID;

        } else {
            //CREATE MODE
            $params = array(
                $this->vcTitle,
                time(),
                $this->txContent,
                $this->vcTitle,
            );

            $sql = "INSERT INTO comment (" .
                "vcTitle, " .
                "daCreated, " .
                "txContent, " .
                "vcTitle) " .
                "VALUES(?,?,?,?)";
            showme($_POST);
            //exit();
            $this->db->_query($sql, $params);

            return $this->db->_getinsertid();

        }

    }

    /**
     * Delete item
     */

    public
    function delete() {
        $params = array($this->iCommentID);

        $sql = "UPDATE comment SET " .
            "iDeleted = 1 " .
            "WHERE iCommentID = ? ";
        $this->db->_query($sql, $params);
    }
}