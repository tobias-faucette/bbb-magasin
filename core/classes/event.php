<?php

/**
 * Created City PhpStorm.
 * Event: tn116
 * Date: 17-05-2017
 * Time: 09:21
 */
class event
{
    public $db;
    public $iEventID;
    public $iVenueID;
    public $vcTitle;
    public $daStart;
    public $daStop;
    public $daCreated;
    public $txShortDesc;
    public $txLongDesc;
    public $vcTicketUrl;
    public $iIsActive;
    public $vcImageUrl;
    public $vcLocation;
    public $vcCatagory;
    public $iDeleted;

    public function __construct() {
        global $db;
        $this->db = $db;


        $this->arrLabels = array(
            "iEventID" => "ID",
            "vcTitle" => "Title",
            "iVenueID" => "Venue",
            "daStart" => "Start Date",
            "daStop" => "Stop Date",
            "daCreated" => "Oprettet",
            "txShortDesc" => "Short Description",
            "txLongDesc" => "Long Description",
            "vcTicketUrl" => "URL til billetkøb",
            "iIsActive" => "Active",
            "vcImageUrl" => "Billede",
            "vcLocation" => "Lokation",
            "vcCatagory" => "Katagori",


        );

        /**
         * Array for formfields:
         * Index = fieldname
         * Value[0] = formtype
         * Value[1] = filter_type
         * Value[2] = Required Status (TRUE/FALSE)
         * Value[3] = Default value
         */
        $this->arrFormElms = array(
            "iEventID" => array("hidden", FILTER_VALIDATE_INT, FALSE, 0),
            "vcTitle" => array("text", FILTER_SANITIZE_STRING, TRUE, "hej"),
            "iVenueID" => array("select", FILTER_VALIDATE_INT, FALSE, 0),
            "daStart" => array("datetime", FILTER_SANITIZE_NUMBER_INT, TRUE, 0),
            "daStop" => array("datetime", FILTER_SANITIZE_NUMBER_INT, TRUE, 0),
            "daCreated" => array("hidden", FILTER_VALIDATE_INT, FALSE, ""),
            "txShortDesc" => array("textarea", FILTER_SANITIZE_STRING, FALSE, ""),
            "txLongDesc" => array("textarea", FILTER_SANITIZE_STRING, FALSE, ""),
            "vcTicketUrl" => array("text", FILTER_SANITIZE_STRING, FALSE, ""),
            "iIsActive" => array("checkbox", FILTER_SANITIZE_STRING, FALSE, "0"),
            "vcImageUrl" => array("file", FILTER_SANITIZE_STRING, TRUE, ""),
            "vcLocation" => array("text", FILTER_SANITIZE_STRING, FALSE, ""),
            "vcCatagory" => array("text", FILTER_SANITIZE_STRING, FALSE, ""),


        );

        $this->arrValues = array();
    }

    /**
     * function to get list of EventsR
     * @return array
     */

    public function getlist() {  //function = method
        $sql = "SELECT * FROM event WHERE iDeleted = 0";

        return $this->db->_fetch_array($sql); //gets all rows, fetch value will take out a single row.

    }


    /**
     * function to get a single Event
     * @param $iEventID
     * @return array
     */
    public function getEvent($iEventID) { //set parameter iEventID to get a single Event
        $this->iEventID = $iEventID;
        $sql = "SELECT * FROM event WHERE iEventID = ? AND iDeleted = 0";
        $row = $this->db->_fetch_array($sql, array($this->iEventID));
        foreach ($row[0] as $key => $value) {
            $this->$key = $value;
        }

        return $row;
        //showme($row);
    }


    /**
     * @return int
     *
     */
    public function save() {
        if ($this->iEventID > 0) {
            //UPDATE MODE
            $params = array(
                $this->vcTitle,
                $this->iVenueID,
                $this->daStart,
                $this->daStop,
                $this->daCreated,
                $this->txShortDesc,
                $this->txLongDesc,
                $this->vcTicketUrl,
                $this->iIsActive,
                $this->vcImageUrl,
                $this->vcLocation,
                $this->vcCatagory,
                $this->iEventID,
            );

            $sql = "UPDATE event SET " .
                "vcTitle = ?, " .
                "iVenueID = ?, " .
                "daStart = ?, " .
                "daStop = ?, " .
                "daCreated = ?, " .
                "txShortDesc = ?, " .
                "txLongDesc = ?, " .
                "vcTicketUrl = ?, " .
                "iIsActive = ?, " .
                "vcImageUrl = ?, " .
                "vcLocation = ?, " .
                "vcCatagory = ? " .
                "WHERE iEventID = ? ";

            $this->db->_query($sql, $params);
            return $this->iEventID;

        } else {
            //CREATE MODE
            $params = array(
                $this->vcTitle,
                $this->iVenueID,
                $this->daStart,
                $this->daStop,
                time(),
                $this->txShortDesc,
                $this->txLongDesc,
                $this->vcTicketUrl,
                $this->iIsActive,
                $this->vcImageUrl,
                $this->vcLocation,
                $this->vcCatagory,
            );

            $sql = "INSERT INTO event (" .
                "vcTitle, " .
                "iVenueID, " .
                "daStart, " .
                "daStop, " .
                "daCreated, " .
                "txShortDesc, " .
                "txLongDesc, " .
                "vcTicketUrl, " .
                "iIsActive, " .
                "vcImageUrl, " .
                "vcLocation, " .
                "vcCatagory) " .
                "VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
            showme($_POST);
            //exit();
            $this->db->_query($sql, $params);

            return $this->db->_getinsertid();

        }

    }

    public function delete() {
        $params = array($this->iEventID);

        $sql = "UPDATE event set " .
            "iDeleted = 1 " .
            "WHERE iEventID = ? ";
        $this->db->_query($sql, $params);

    }

}

