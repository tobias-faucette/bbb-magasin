<?php

class shopcart
{
    public $db;
    public $arrLabels;
    public $arrFormElms;
    public $arrValues;
    public $iCartID;
    public $iUserID;
    public $vcSessionID;
    public $daCreated;


    public function __construct() {
        global $db;
        $this->db = $db;


        $this->arrLabels = array(
            "iCartID" => "ID",
            "iUser" => "User",
            "vcSessionID" => "Session ID",
            "daCreated" => "Created",

        );

        /**
         * Array for formfields:
         * Index = fieldname
         * Value[0] = formtype
         * Value[1] = filter_type
         * Value[2] = Required Status (TRUE/FALSE)
         * Value[3] = Default value
         */
        $this->arrFormElms = array(
            "iCartID" => array("hidden", FILTER_VALIDATE_INT, FALSE, 0),
            "iUserID" => array("text", FILTER_VALIDATE_INT, TRUE, ""),
            "vcSessionID" => array("text", FILTER_VALIDATE_INT, TRUE, ""),
            "daCreated" => array("number", FILTER_VALIDATE_INT, TRUE, ""),


        );

        $this->arrValues = array();
    }


    /**
     * Select single item
     * selects iUserID if logged in, if not it takes sessionID
     * @param $iUserID
     * @return int iCartID
     */
    public function getItemByUser($iUserID) {
        $strSelectCart = "SELECT iCartID FROM shopcart " .
            " WHERE ";

        if ($iUserID) {

            $strSelectSession = "SELECT iCartID FROM shopcart " .
                "WHERE vcSessionID = ? " .
                "AND iUserID =0";

            if ($this->db->_fetch_value($strSelectSession, array(session_id()))) {
                $strUpdateSession = "UPDATE " . $this->dbTable . " SET " .
                    "iUserID = ?" .
                    "WHERE vcSessionID = ?";

                $this->_query($strUpdateSession, array(session_id()));
            }

            $strSelectCart .= " iUserID = ?";
            $params = array($iUserID);

        } else {
            $strSelectCart .= " vcSessionID = ?";
            $params = array(session_id());
        }

        $iCartID = $this->db->_fetch_value($strSelectCart, $params);
        $this->getItem($iCartID);
        return $iCartID;
    }


    public function getItem($iCartID) {
        $this->iCartID = $iCartID;
        $this->arrValues["arrCartLines"] = $this->getCartLines($iCartID);
        $sql = "SELECT * FROM shopcart WHERE iCartID = ?";
        $row = $this->db->_fetch_array($sql, array($this->iCartID));


        foreach ($row as $key => $value) {
            $this->$key = $value;
        }
    }


    /**
     * Select cart lines
     * @param int $iItemID
     * @return array lines
     */
    public function getCartLines($iCartID) {
        $params = array($iCartID);
        $strSelect = "SELECT * FROM shopcartline WHERE iCartID = ?";
        return $this->db->_fetch_array($strSelect, $params);
    }


    /**
     * Remove product
     * @param int $iUserID
     * @return mixed
     */
    public function create($iUserID = 0) {
        $sql = "INSERT INTO shopcart(iUserID, vcSessionID, daCreated) VALUES(?,?,?)";
        $this->db->_query($sql, array($iUserID, session_id(), time()));
        return $this->db->_getinsertid();
    }


    /**
     * remove product
     */

    public function removeProduct($iProductID) {
        $sql = "DELETE FROM shopcartline WHERE iCartID = ? AND iProductID = ?";
        $this->db->_query($sql, array($this->iCartID, $iProductID));
    }


    /**
     * add product
     * @param $iProductID
     * @param $iQuantity
     */

    public function addProduct($iProductID, $iQuantity) {
        $strInsert = "INSERT INTO shopcartline(iCartID, iProductID, iQuantity) VALUES(?,?,?)";
        $this->db->_query($strInsert, array($this->iCartID, $iProductID, $iQuantity));
    }


    /**
     * update product
     * @param $iCartLineID
     * @param $iQuantity
     */
    public function updateProduct($iCartLineID, $iQuantity) {
        $strInsert = "UPDATE shopcartline SET iQuantity = ? WHERE iCartID = ? AND iCartLineID = ?";
        $this->db->_query($strInsert, array($iQuantity, $this->iCartID, $iCartLineID));
    }


    /**
     * fetch quantity sum from cart lines
     * @return int iQuantity summary
     */

    public function getCartQuantity() {
        $num = 0;
        if (isset($this->iCartID) && $this->iCartID > 0) {
            $strSelectCart = "SELECT SUM(iQuantity) FROM shopcartline WHERE iCartID = ?";
            $num = $this->db->_fetch_value($strSelectCart, array($this->iCartID));
        }
        return $num;

    }


    /**
     * get total of products in cart
     * @return int|type
     */

    public function getCartTotal() {
        $total = 0;
        if (isset($this->iCartID) && $this->iCartID > 0) {
            $strSelectCart = "SELECT SUM(l.iQuantity * p.iPrice) FROM shopcartline l JOIN shopproduct p ON l.iProductID = p.iProductID WHERE iCartID = ?";
            $total = $this->db->_fetch_value($strSelectCart, array($this->iCartID));
        }
        return $total;

    }


    public function getAll() {
        $this->arrLabels = array(
            "opts" => "Options",
            "vcTitle" => $this->arrColumns["vcTitle"]["Label"],
            "vcProductNumber" => $this->arrColumns["vcProductNumber"]["Label"],
        );
        $strSelect = "SELECT * " .
            "FROM " . $this->dbTable . " " .
            "WHERE iDeleted = 0";
        return $this->db->_fetch_array($strSelect);
    }


    /**
     * Get product related groups
     * @return array
     */

    public function getGroupRelations() {
        $params = array($this->arrValues["iProductID"]);
        $strSelect = "SELECT c.iCategoryID, c.vcTitle " .
            " FROM shopcategory c " .
            "JOIN shopcatprodrel x ON x.iCategoryID = c.iCategoryID " .
            "WHERE x.iProductID = ? " .
            "AND c.iDeleted = 0";
        return $this->db->_fetch_array($strSelect, $params);
    }

}